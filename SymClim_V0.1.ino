#include "DHT.h"
#include "DS3231.h"
#include <Wire.h>
#include <LCD.h>
#include <LiquidCrystal.h>
#include <LiquidCrystal_I2C.h>
#include <LiquidCrystal_SI2C.h>
#include <Keypad_I2C.h>
#include <Keypad.h>
#define I2CADDR 0x21
DHT dhtd, dhtm, dhtu;
const byte ROWS = 1;
const byte COLS = 5;
char keys[ROWS][COLS] = {{'L', 'P', 'D', 'R', 'U'}};
byte rowPins[ROWS] = {3};
byte colPins[COLS] = {0, 1, 2, 4, 5};
unsigned long previousMillis = 0;
unsigned long previousMillis1 = 0;
unsigned long previousMillis2 = 0;
short wd;
short at;
short ah;
short losowa = 0;
short pwm = 0;
short maks = 255;
short mini = 51;
short bl = 0;
short T1 = 0;
short T2 = 0;
short T3 = 0;


Keypad_I2C kpd( makeKeymap(keys), rowPins, colPins, ROWS, COLS, I2CADDR, PCF8574 );
LiquidCrystal_I2C lcd(0x27, 2, 1, 0, 4, 5, 6, 7, 3, POSITIVE);
byte ictemp[] = {B00100, B01010, B01010, B01110, B01110, B11111, B11111, B01110};
byte ichumi[] = {B00100, B00100, B01010, B01010, B10001, B10001, B10001, B01110};
byte iccels[] = {B11000, B11000, B00110, B01001, B01000, B01000, B01001, B00110};
RTClib RTC;

void setup()
{
  Serial.begin(9600);
  Wire.begin();
  kpd.begin( makeKeymap(keys) );
  lcd.begin(16, 2);
  lcd.createChar(0, ictemp);
  lcd.createChar(1, ichumi);
  lcd.createChar(2, iccels);
  lcd.clear();
  dhtd.setup(2); dhtm.setup(4); dhtu.setup(7);
  wd = 2;
  pinMode(A0, OUTPUT);
  pinMode(A2, OUTPUT);
  pinMode(3, OUTPUT);
  pinMode(5, OUTPUT);
  pinMode(6, OUTPUT);
  pinMode(8, OUTPUT);
  analogWrite(6, 120);
  digitalWrite(8, HIGH);
  analogWrite(A0, HIGH);


}
void loop()
{
  unsigned long currentMillis = millis();
  DateTime now = RTC.now();
  char key = kpd.getKey();

  if (currentMillis - previousMillis >= 5000)
  {
    previousMillis = currentMillis;
    bl++;
    if (((dhtd.getTemperature() + dhtm.getTemperature() + dhtu.getTemperature()) / 3) > 0)at = (dhtd.getTemperature() + dhtm.getTemperature() + dhtu.getTemperature()) / 3;
    if (((dhtd.getHumidity() + dhtm.getHumidity() + dhtu.getHumidity()) / 3) > 0)ah = (dhtd.getHumidity() + dhtm.getHumidity() + dhtu.getHumidity()) / 3;


    if (wd == 2)
    {

      wd = 4;

      short t = dhtd.getTemperature();
      short h = dhtd.getHumidity();
      if (t == 0 && h == 0) {}
      else
      {
        T1 = t;
        lcd.clear();
        lcd.setCursor(0, 0); lcd.write(byte(0)); lcd.setCursor(3, 0); lcd.write(byte(2)); lcd.setCursor(5, 0); lcd.write(byte(1)); lcd.setCursor(8, 0); lcd.print('%');
        lcd.setCursor(1, 0); lcd.print(t); lcd.setCursor(6, 0); lcd.print(h); lcd.setCursor(12, 0); lcd.print("DOWN");
        lcd.setCursor(0, 1); lcd.write(byte(0)); lcd.setCursor(3, 1); lcd.write(byte(2)); lcd.setCursor(5, 1); lcd.write(byte(1)); lcd.setCursor(8, 1); lcd.print('%');
        lcd.setCursor(1, 1); lcd.print(at); lcd.setCursor(6, 1); lcd.print(ah);
      }
    }
    else if (wd == 4)
    {

      wd = 7;
      short t = dhtm.getTemperature();
      short h = dhtm.getHumidity();
      if (t == 0 && h == 0) {}
      else
      {
        T2 = t;
        lcd.clear();
        lcd.setCursor(0, 0); lcd.write(byte(0)); lcd.setCursor(3, 0); lcd.write(byte(2)); lcd.setCursor(5, 0); lcd.write(byte(1)); lcd.setCursor(8, 0); lcd.print('%');
        lcd.setCursor(1, 0); lcd.print(t); lcd.setCursor(6, 0); lcd.print(h); lcd.setCursor(10, 0); lcd.print("MIDDLE");
        lcd.setCursor(0, 1); lcd.write(byte(0)); lcd.setCursor(3, 1); lcd.write(byte(2)); lcd.setCursor(5, 1); lcd.write(byte(1)); lcd.setCursor(8, 1); lcd.print('%');
        lcd.setCursor(1, 1); lcd.print(at); lcd.setCursor(6, 1); lcd.print(ah);
      }
    }
    else if (wd == 7)
    {

      wd = 2;
      short t = dhtu.getTemperature();
      short h = dhtu.getHumidity();
      if (t == 0 && h == 0) {}
      else
      {
        T3 = t;
        lcd.clear();
        lcd.setCursor(0, 0); lcd.write(byte(0)); lcd.setCursor(3, 0); lcd.write(byte(2)); lcd.setCursor(5, 0); lcd.write(byte(1)); lcd.setCursor(8, 0); lcd.print('%');
        lcd.setCursor(1, 0); lcd.print(t); lcd.setCursor(6, 0); lcd.print(h); lcd.setCursor(14, 0); lcd.print("UP");
        lcd.setCursor(0, 1); lcd.write(byte(0)); lcd.setCursor(3, 1); lcd.write(byte(2)); lcd.setCursor(5, 1); lcd.write(byte(1)); lcd.setCursor(8, 1); lcd.print('%');
        lcd.setCursor(1, 1); lcd.print(at); lcd.setCursor(6, 1); lcd.print(ah);
      }
    }
    if (bl == 6)digitalWrite(8, LOW);

  }
  if (pwm == losowa)losowa = random(mini, maks);
  if (now.hour() == 7 && currentMillis - previousMillis1 >= 14117)
  {
    previousMillis1 = currentMillis;

    if (maks < 255)maks++;
    if (maks >= 5)
      mini = maks / 5;
    else
    {
      mini = 1;
    }
  }

  if (now.hour() == 19 && currentMillis - previousMillis1 >= 14000)
  {
    previousMillis1 = currentMillis;
    if (maks > 0)maks--;
    if (maks >= 5)
      mini = maks / 5;
    else
    {
      mini = 1;
    }
    if (maks == 0)mini = 0;
  }
  if (currentMillis - previousMillis2 >= 125)
  {
    previousMillis2 = currentMillis;
    if (mini < 15)
    {
      losowa = mini;
    }
    if (losowa > pwm)pwm++;
    if (losowa < pwm)pwm--;
    analogWrite(5, pwm);





  }
  if (7 <= now.hour() && now.hour() <= 19) //dzien
  {
    if (ah < 45 && ah > 0)digitalWrite(A0, LOW);
    else digitalWrite(A0, HIGH);
    if (ah < 55)
    {
      analogWrite(3, 0);

    }
    else
    {
      short x = (15 * (ah - 55)) + 105;
      if (x > 255)analogWrite(3, 255);
      else analogWrite(3, x);

    }
    if (T1 > 30 || T2 > 30 || T3 > 30)digitalWrite(A2, HIGH);
    else digitalWrite(A2, LOW);
  }
  if (19 < now.hour() || now.hour() < 7) //noc
  {
    maks = 0;
    mini = 0;
    analogWrite(5, 0);
    if (ah < 55 && ah > 0)digitalWrite(A0, LOW);
    else digitalWrite(A0, HIGH);
    if (ah < 65)
    {
      analogWrite(3, 0);
    }
    else
    {
      short y = (15 * (ah - 65)) + 135;
      if (y > 255)analogWrite(3, 255);
      else analogWrite(3, y);

    }
    if (T1 > 22 || T2 > 22 || T3 > 22)digitalWrite(A2, HIGH);
    else digitalWrite(A2, LOW);
  }
  if (now.hour() > 20 || now.hour() < 7)
  {

  }

  if (key)
  {
    bl = 0;
    digitalWrite(8, HIGH);
  }

}
