#include "Adafruit_Keypad.h"
#include <Wire.h>
#include "DHT.h"
#include "DS3231.h"
#include <LiquidCrystal_I2C.h>



DHT dhtu, dhtm, dhtd;
RTClib myRTC;

LiquidCrystal_I2C lcd(0x27, 2, 1, 0, 4, 5, 6, 7, 3, POSITIVE);
byte ictemp[] = {B00100, B01010, B01010, B01110, B01110, B11111, B11111, B01110};
byte ichumi[] = {B00100, B00100, B01010, B01010, B10001, B10001, B10001, B01110};
byte iccels[] = {B11000, B11000, B00110, B01001, B01000, B01000, B01001, B00110};
byte icempt[] = {B00000, B01110, B11011, B10001, B10001, B11011, B01110, B00000};
byte icfull[] = {B00000, B00000, B01110, B01110, B01110, B01110, B00000, B00000};
byte icleft[] = {B00010, B00110, B01110, B11111, B11111, B01110, B00110, B00010};
byte icrigh[] = {B01000, B01100, B01110, B11111, B11111, B01110, B01100, B01000};

const byte ROWS = 1;
const byte COLS = 5;
char keys[ROWS][COLS] = {'U','R','D','O','L'};
byte rowPins[ROWS] = {PA5};
byte colPins[COLS] = {PA4, PA3, PA2, PA1,PA0};
Adafruit_Keypad customKeypad = Adafruit_Keypad( makeKeymap(keys), rowPins, colPins, ROWS, COLS);
bool IsDay, sunsetTime, sunriseTime;

short fo, ft, fn, fw, menu, sensorNr, bl, cloudy, light, minVal, maxVal, nextMonth, setTemperature, Tu, Tm, Td, Hu, Hm, Hd;
long clk_A, clk_B, clk_1s, clk_5s, hour, minute, second;
long klimat[8][12] = {
  { 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 },                                      //0-Liczba dni
  { 32, 30, 29, 27, 24, 21, 20, 22, 26, 29, 30, 31 },                                      //1-Maksymalna temperatura
  { 27, 26, 25, 22, 19, 18, 17, 18, 21, 23, 24, 26 },                                      //2-Średnia dobowa maksymalna
  { 20, 19, 18, 15, 12, 10,  9, 10, 12, 15, 17, 19 },                                      //3-Średnia dobowa minimalna
  { 25, 28, 27, 25, 22, 21, 19, 17, 18, 23, 27, 25 },                                      //4-Prawdopodobieństwo opadów
  { 76, 98, 79, 77, 69, 65, 50, 48, 47, 62, 71, 67 },                                      //5-Średni opad
  { 17820, 18960, 20520, 22020, 23340, 24660, 25260, 24480, 22440, 19980, 17700, 16560 },  //6-Świt(s)
  { 72540, 72000, 70320, 67740, 65700, 64440, 64560, 65700, 66960, 68220, 69720, 71700 }}; //7-Zmierzch(s)



void setup() 
{
  customKeypad.begin();
  Wire.begin();  
  Wire.setClock(1000);
  lcd.begin(16, 2);
  lcd.backlight(); 
  lcd.createChar(0, ictemp); lcd.createChar(1, ichumi); lcd.createChar(2, iccels); lcd.createChar(3, icempt); lcd.createChar(4, icfull); lcd.createChar(5, icleft); lcd.createChar(6, icrigh);
  
  dhtu.setup(PA11); dhtm.setup(PA12); dhtd.setup(PA15);
  
  pinMode(PB3, OUTPUT); pinMode(PB4, OUTPUT); pinMode(PA10, OUTPUT); pinMode(PA6, OUTPUT); pinMode(PB8, OUTPUT); pinMode(PB9, OUTPUT);
  digitalWrite(PB3, 1); digitalWrite(PB4, 1);

  IsDay = sunsetTime = sunriseTime = false;
  Tu = Tm = Td = Hu = Hm = Hd = 0;
  clk_A = clk_B = clk_1s = clk_5s = bl = minVal = maxVal = light = cloudy = setTemperature = 0;
  fo = ft = fn = fw = sensorNr = menu= 1;
}
  
void loop() 
{
  DateTime now = myRTC.now();
  hour=now.hour(); minute=now.minute(); second=now.second();
  
  customKeypad.tick();
  keypadEvent e = customKeypad.read();


    /*----------Sterowanie wyświetlaczem----------*/

  if(e.bit.EVENT == KEY_JUST_PRESSED)
  {
    if((char)e.bit.KEY=='D')
    {        
      if(menu==5)menu=1;
      else menu++;
      //lcd.clear();
    }    
  }
  
  if(e.bit.EVENT == KEY_JUST_PRESSED)
  {
    if((char)e.bit.KEY=='U')
    {
      if(menu==1)menu=5;
      else menu--;
      //lcd.clear();
    }     
  }




  if(e.bit.EVENT == KEY_JUST_PRESSED)
  {
    bl=0;
    lcd.clear();
    lcd.backlight();    
  }
    
  if (now.month() == 12) nextMonth = 0;
  else nextMonth = now.month();

  /*----------Zegar - interwał 1s----------*/

  if ((hour * 3600) + (minute * 60) + second !=clk_1s)
  {
    clk_1s = (hour * 3600) + (minute * 60) + second;
    bl++;
    if(bl>=30)//------------------------------------------------------------------Czas podświetlania LCD 30s
    {
      lcd.noBacklight();      
      menu = 1;
    }

    if (dhtu.getTemperature() > 0)Tu = dhtu.getTemperature(); //------------------Odczyt danych z DHT
    if (dhtm.getTemperature() > 0)Tm = dhtm.getTemperature(); //
    if (dhtd.getTemperature() > 0)Td = dhtd.getTemperature(); //
    if (dhtu.getHumidity() > 0)Hu = dhtu.getHumidity();       //
    if (dhtm.getHumidity() > 0)Hm = dhtm.getHumidity();       //
    if (dhtd.getHumidity() > 0)Hd = dhtd.getHumidity();       //
  }
  /*--------------------*/



  
  

  if(menu==1)//-------------------------------------------------------------------Menu - Wyświetlacz główny
  {
    mainScreen();
    if (((hour * 3600) + (minute * 60) + second)-clk_5s>=5)
    {
      clk_5s = (hour * 3600) + (minute * 60) + second;
      if(sensorNr == 1)//---------------------------------------------------------Wyświetlanie danych z górnego czujnika
      {
        sensorNr = 2;
        
        lcd.clear();
        mainScreen();
        lcd.setCursor(12, 0);lcd.print("GORA");
        if (Tu > 0 && Hu > 0)
        {    
          lcd.setCursor(1, 0); lcd.print(Tu);
          lcd.setCursor(6, 0); lcd.print(Hu);

          if (((Tu + Tm + Td) / 3) > 0)
          {
            lcd.setCursor(1, 1); 
            lcd.print((Tu + Tm + Td) / 3);
          }
          if (((Hu + Hm + Hd) / 3) > 0)
          {
            lcd.setCursor(6, 1);
            lcd.print((Hu + Hm + Hd) / 3);
          }
        }
      }

      else if(sensorNr == 2)//----------------------------------------------------Wyświetlanie danych z środkowego czujnika
      {
        sensorNr = 3;
        
        lcd.clear();
        mainScreen();
        lcd.setCursor(12, 0);lcd.print("SROD");
        if (Tm > 0 && Hm > 0)
        {          
          lcd.setCursor(1, 0); lcd.print(Tm);
          lcd.setCursor(6, 0); lcd.print(Hm);

          if (((Tu + Tm + Td) / 3) > 0)
          {
            lcd.setCursor(1, 1); 
            lcd.print((Tu + Tm + Td) / 3);
          }
          if (((Hu + Hm + Hd) / 3) > 0)
          {
            lcd.setCursor(6, 1);
            lcd.print((Hu + Hm + Hd) / 3);
          }
        }
      }

      else if(sensorNr == 3)//----------------------------------------------------Wyświetlanie danych z dolnego czujnika
      {
        sensorNr = 1;
        
        lcd.clear();
        mainScreen();
        lcd.setCursor(12, 0);lcd.print("DOL ");
        if (Td > 0 && Hd > 0)
        {          
          lcd.setCursor(1, 0); lcd.print(Td);
          lcd.setCursor(6, 0); lcd.print(Hd);

          if (((Tu + Tm + Td) / 3) > 0)
          {
            lcd.setCursor(1, 1); 
            lcd.print((Tu + Tm + Td) / 3);
          }
          if (((Hu + Hm + Hd) / 3) > 0)
          {
            lcd.setCursor(6, 1);
            lcd.print((Hu + Hm + Hd) / 3);
          }
        }
      }
    }
  }
  /*--------------------*/


  /*----------Sterowanie oświetleniem----------*/

  if (klimat[6][now.month() - 1] - klimat[6][nextMonth] < 0)//--------------------Godzina wschodu, gdy długość dnia maleje + pory dnia
  {
    if ((hour * 3600) + (minute * 60) + second == klimat[6][now.month() - 1] + (now.day() - 1) * (abs(klimat[6][now.month() - 1] - klimat[6][nextMonth]) / klimat[0][now.month() - 1])) sunriseTime = true;
    if ((hour * 3600) + (minute * 60) + second >= klimat[6][now.month() - 1] + (now.day() - 1) * (abs(klimat[6][now.month() - 1] - klimat[6][nextMonth]) / klimat[0][now.month() - 1])&&(hour * 3600) + (minute * 60) + second <= klimat[7][now.month() - 1] - (now.day() - 1) * (abs(klimat[7][now.month() - 1] - klimat[7][nextMonth]) / klimat[0][now.month() - 1])) IsDay = true;
    else IsDay = false;
  } 
  else  //------------------------------------------------------------------------Godzina wschodu, gdy długość dnia rośnie + pory dnia
  {
    if ((hour * 3600) + (minute * 60) + second == klimat[6][now.month() - 1] - (now.day() - 1) * (abs(klimat[6][now.month() - 1] - klimat[6][nextMonth]) / klimat[0][now.month() - 1])) sunriseTime = true;
    if ((hour * 3600) + (minute * 60) + second >= klimat[6][now.month() - 1] - (now.day() - 1) * (abs(klimat[6][now.month() - 1] - klimat[6][nextMonth]) / klimat[0][now.month() - 1])&&(hour * 3600) + (minute * 60) + second <= klimat[7][now.month() - 1] + (now.day() - 1) * (abs(klimat[7][now.month() - 1] - klimat[7][nextMonth]) / klimat[0][now.month() - 1])) IsDay = true;
    else IsDay = false;
  }
 
  if (klimat[7][now.month() - 1] - klimat[7][nextMonth] < 0)  //------------------Godzina zachodu, gdy długość dnia maleje
  {
    if ((hour * 3600) + (minute * 60) + second == klimat[7][now.month() - 1] + (now.day() - 1) * (abs(klimat[7][now.month() - 1] - klimat[7][nextMonth]) / klimat[0][now.month() - 1])) sunsetTime = true;
  } 
  else  //------------------------------------------------------------------------Godzina zachodu, gdy długość dnia rośnie
  {
    if ((hour * 3600) + (minute * 60) + second == klimat[7][now.month() - 1] - (now.day() - 1) * (abs(klimat[7][now.month() - 1] - klimat[7][nextMonth]) / klimat[0][now.month() - 1])) sunsetTime = true;
  }  
  
  if(sunriseTime==false&&IsDay==true)//-------------------------------------------Zabezpieczenie pory dnia w przypadku chwilowego braku zasilania
  {
    minVal=51;
    maxVal=255;
  }

  if (light == cloudy) cloudy = random(minVal, maxVal);//-------------------------Symulacja zachmurzenia
  if ((hour * 3600) + (minute * 60) + second !=clk_B) 
  {
    clk_B = (hour * 3600) + (minute * 60) + second;
    if (minVal < 10) cloudy = maxVal;
    if (cloudy > light) light++;
    if (cloudy < light) light--;
  }
  
  if (sunriseTime == true && ((hour * 3600) + (minute * 60) + second) - clk_A >= 20)//--Symulacja wschodu
  {
    clk_A = (hour * 3600) + (minute * 60) + second;
    if (maxVal < 255) maxVal++;
    if (maxVal == 255) sunriseTime = false;
    if (maxVal >= 5) minVal = maxVal / 5;
    else minVal = 1;
  }
  

  if(sunsetTime == true&& ((hour * 3600) + (minute * 60) + second) - clk_A >= 20)//--Symulacja zachodu
  {
    clk_A = (hour * 3600) + (minute * 60) + second;
    if(maxVal>0)maxVal--;
    if(maxVal==0)sunsetTime = false;
    if(maxVal>=5) minVal = maxVal/5;
    else minVal = 1;
    if(maxVal==0)minVal = 0;
  }

  if(menu==2)//-------------------------------------------------------------------Menu - Oświetlenie
  {    
    oswietlenie();
    if(e.bit.EVENT == KEY_JUST_PRESSED)
    {
      if((char)e.bit.KEY=='L')
        if(fo>0) fo--;   
    }
    
    if(e.bit.EVENT == KEY_JUST_PRESSED)
    {
      if((char)e.bit.KEY=='R')
        if(fo<2) fo++;
    }
    
      

    if (fo == 0)//----------------------------------------------------------------Oświetlenie - Włączone 
    {
      lcd.setCursor(0, 1); lcd.write(byte(3));
      analogWrite(PB8, 255);
    }

    if (fo == 1)//----------------------------------------------------------------Oświetlenie - Sterowanie automatyczne
    {
      lcd.setCursor(5, 1); lcd.write(byte(3));
      analogWrite(PB8, light);
    }

    if (fo == 2)//----------------------------------------------------------------Oświetlenie - Wyłączone 
    {
      lcd.setCursor(12, 1); lcd.write(byte(3));
      analogWrite(PB8, 0);
    }
  }

  /*--------------------*/


  /*----------Sterowanie grzaniem----------*/

  if(klimat[3][now.month()-1]-klimat[3][nextMonth]>0)//---------------------------Jeżeli temperatura w kolejnym miesiącu maleje
  {
    if(IsDay==true)
    {
      if(setTemperature < klimat[2][now.month()-1])//-----------------------------Ustawienie temperatury na dzień
        setTemperature = random(klimat[2][now.month()-1] - ((klimat[2][now.month()-1]-klimat[2][nextMonth])*now.day())/klimat[0][now.month()-1],(klimat[1][now.month()-1] - ((klimat[1][now.month()-1]-klimat[1][nextMonth])*now.day())/klimat[0][now.month()-1])+1);
    } 
    else//------------------------------------------------------------------------Ustawienie temperatury na noc
    {
      setTemperature = klimat[3][now.month()-1]- ((klimat[3][now.month()-1]-klimat[3][nextMonth])*now.day())/klimat[0][now.month()-1];
    }
  
  }
  else//--------------------------------------------------------------------------Jeżeli temperatura w kolejnym miesiącu rośnie
  { 
    if(IsDay==true) 
    {
      if(setTemperature < klimat[2][now.month()-1])//-----------------------------Ustawienie temperatury na dzień
        setTemperature = random(klimat[2][now.month()-1] + ((klimat[2][now.month()-1]-klimat[2][nextMonth])*now.day())/klimat[0][now.month()-1],(klimat[1][now.month()-1] + ((klimat[1][now.month()-1]-klimat[1][nextMonth])*now.day())/klimat[0][now.month()-1])+1);
    } 
    else//------------------------------------------------------------------------Ustawienie temperatury na noc
    {
      setTemperature = klimat[3][now.month()-1] + ((klimat[3][now.month()-1]-klimat[3][nextMonth])*now.day())/klimat[0][now.month()-1];
    }
  }

  if(menu==3)//-------------------------------------------------------------------Menu - Ogrzewanie
  {
    ogrzewanie();
    if(e.bit.EVENT == KEY_JUST_PRESSED)
    {
      if((char)e.bit.KEY=='L')
      if(ft>0) ft--;  
    }      
      
    if(e.bit.EVENT == KEY_JUST_PRESSED)
    {
      if((char)e.bit.KEY=='R')
        if(ft<2) ft++; 
    }      

    if (ft == 0)//----------------------------------------------------------------Ogrzewanie - Włączone 
    {
      lcd.setCursor(0, 1); lcd.write(byte(3));
      digitalWrite(PB4,0);
    }

    if (ft == 1)//----------------------------------------------------------------Ogrzewanie - Sterowanie automatyczne
    {
      lcd.setCursor(5, 1); lcd.write(byte(3));
      if((Tu+Tm+Td)/3<setTemperature) digitalWrite(PB4, 0);
      if((Tu+Tm+Td)/3>setTemperature) digitalWrite(PB4, 1);
    }

    if (ft == 2)//----------------------------------------------------------------Ogrzewanie - Wyłączone 
    {
      lcd.setCursor(12, 1); lcd.write(byte(3));
      digitalWrite(PB4,1);
    }
  }
  
  /*--------------------*/


  /*----------Sterowanie wentylacją i wilgocią----------*/

  if(menu==4)//-------------------------------------------------------------------Menu - Nawilżacz
  {    
    wilgotnosc();
    if(e.bit.EVENT == KEY_JUST_PRESSED)
    {
      if((char)e.bit.KEY=='L')
        if(fn>0) fn--;    
    }
      
      
    if(e.bit.EVENT == KEY_JUST_PRESSED)
    {
      if((char)e.bit.KEY=='R')
        if(fn<2) fn++;   
    }      

    if (fn == 0)//----------------------------------------------------------------Nawilżacz - Włączone 
    {
      lcd.setCursor(0, 1); lcd.write(byte(3));
      digitalWrite(PB3,0);
    }

    if (fn == 1)//----------------------------------------------------------------Nawilżacz - Sterowanie automatyczne
    {
      lcd.setCursor(5, 1); lcd.write(byte(3));
      if(IsDay==true)//-----------------------------------------------------------Jeżeli jest dzień
      {
        if((Hu+Hm+Hd)/3<40) digitalWrite(PB3, 0);//-------------------------------Włączenie nawilżacza gdzy wilgotność jest poniżej 40%
          else digitalWrite(PB3, 1);
      }
      else//----------------------------------------------------------------------Jeżeli jest noc
      {   
          if((Hu+Hm+Hd)/3<60) digitalWrite(PB3, 0);//-----------------------------Włączenie nawilżacza gdzy wilgotność jest poniżej 60%
          else digitalWrite(PB3, 1);
      }
    }

    if (fn == 2)//----------------------------------------------------------------Nawilżacz - Wyłączone 
    {
      lcd.setCursor(12, 1); lcd.write(byte(3));
      digitalWrite(PB3,1);
    }
  }
  /*--------------------*/

  if(menu==5)//-------------------------------------------------------------------Menu - Wentylacja
  {      
    //wentylacja();
    lcd.setCursor(3, 0); lcd.print("WENTYLACJA");
    lcd.setCursor(1, 1); lcd.print("On"); lcd.setCursor(6, 1); lcd.print("Auto"); lcd.setCursor(13, 1); lcd.print("Off");
    if(e.bit.EVENT == KEY_JUST_PRESSED)
    {
      if((char)e.bit.KEY=='L')
        if(fw>0) fw--;    
    }      
     
    if(e.bit.EVENT == KEY_JUST_PRESSED)
    {
      if((char)e.bit.KEY=='R')
        if(fw<2) fw++;
    }      

    if (fw == 0)//----------------------------------------------------------------Wentylacja - Włączone 
    {
      lcd.setCursor(0, 1); lcd.write(byte(3));
      analogWrite(PB9, 255);
    }

    if (fw == 1)//----------------------------------------------------------------Wentylacja - Sterowanie automatyczne
    {
      lcd.setCursor(5, 1); lcd.write(byte(3));
      if(IsDay==true)//-----------------------------------------------------------Jeżeli jest dzień
      {
        if((Hu+Hm+Hd)/3>50)
          analogWrite(PB9, map((Hu+Hm+Hd)/3,51,80,120,255));//--------------------Włączanie wentylacji powyżej 50%
        else
          analogWrite(PB9, 0);
      }
      else//----------------------------------------------------------------------Jeżeli jest noc
      {
        if((Hu+Hm+Hd)/3>70) 
          analogWrite(PB9, map((Hu+Hm+Hd)/3,71,95,120,255));//--------------------Włączanie wentylacji powyżej 70%
        else
          analogWrite(PB9, 0);
      }
    }

    if (fw == 2)//----------------------------------------------------------------Wentylacja - Wyłączone 
    {
      lcd.setCursor(12, 1); lcd.write(byte(3));
      analogWrite(PB9, 0);
    }
  }  
}
  
  /*--------------------*/




void mainScreen()
{
  lcd.setCursor(0, 0); lcd.write(byte(0)); lcd.setCursor(3, 0); lcd.write(byte(2)); lcd.setCursor(5, 0); lcd.write(byte(1)); lcd.setCursor(8, 0); lcd.print('%');
  lcd.setCursor(0, 1); lcd.write(byte(0)); lcd.setCursor(3, 1); lcd.write(byte(2)); lcd.setCursor(5, 1); lcd.write(byte(1)); lcd.setCursor(8, 1); lcd.print('%');
  lcd.setCursor(12, 1); lcd.print("SRED");
}

void oswietlenie()
{
  lcd.setCursor(3, 0); lcd.print("OSWIETLENIE");
  lcd.setCursor(1, 1); lcd.print("On"); lcd.setCursor(6, 1); lcd.print("Auto"); lcd.setCursor(13, 1); lcd.print("Off");
}

void ogrzewanie()
{
  lcd.setCursor(3, 0); lcd.print("OGRZEWANIE");
  lcd.setCursor(1, 1); lcd.print("On"); lcd.setCursor(6, 1); lcd.print("Auto"); lcd.setCursor(13, 1); lcd.print("Off");
}

void wilgotnosc()
{
  lcd.setCursor(3, 0); lcd.print("NAWILZACZ");
  lcd.setCursor(1, 1); lcd.print("On"); lcd.setCursor(6, 1); lcd.print("Auto"); lcd.setCursor(13, 1); lcd.print("Off");
}

void wentylacja()
{
  lcd.setCursor(3, 0); lcd.print("WENTYLACJA");
  lcd.setCursor(1, 1); lcd.print("On"); lcd.setCursor(6, 1); lcd.print("Auto"); lcd.setCursor(13, 1); lcd.print("Off");
}